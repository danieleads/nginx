![nginx logo](https://s3.amazonaws.com/awsmp-logos/nginx300x90.png)
##Speedy Atlassian Apps
This repository is intended to help you quickly set up nginx to frontend your Atlassian applications. While it theoretically should work with the whole Atlassian toolchain, it's only been tested with JIRA and Confluence.

##Notes about configuring nginx
* **conf.d/http.conf** line 4 **server_name** must be updated with the correct URL for your server. This is what bumps http:// connnections up to https://
* **conf.d/ssl.conf** lines 33-34 contain the paths to the SSL certificates. You must add some certificates to this directory and change this file to point at them!
* **conf.d/ssl.conf** line 8 sets the port your Atlassian application is running on. By default, this is set for JIRA. Just change the port number to the one your application is listening on if it's not 8080 (for example, Confluence is 8090 by default)

## Notes about your Atlassian apps
Once nginx is configured to front your Atlassian application, you'll need to tell your application to expect it to to be there.

1. Open **<installdir>/conf/server.xml** for editing
2. Find the existing connector (defined on port 8090 by default on Confluence and 8080 for JIRA)
3. Modify it to add scheme, proxyName, and proxyPort options. Here's a full connector for reference:

		<Connector port="8090"
	           maxThreads="200"
	           minSpareThreads="25"
	           connectionTimeout="20000"
	           enableLookups="false"
	           maxHttpHeaderSize="8192"
	           protocol="HTTP/1.1"
	           useBodyEncodingForURI="true"
	           redirectPort="8443"
	           acceptCount="100"
	           disableUploadTimeout="true"
	           scheme="https"
	           proxyName="APP.DOMAIN.org"
	           proxyPort="443"/>  

4. Double-check that your **proxyName** option matches the URL for your application
5. Restart the application
6. Set the base URL in the application to include **https://** in the front instead of **http://**


##The 500 error page
The configuration (conf.d/ssl.conf) points at the default nginx HTML directory in CentOS of /usr/share/nginx/html for these files. Change the config if you save these files somewhere else.
These files are included here in the **html** folder. This page will display when your application isn't responding (either because it's down or is having a problem), so it's a good idea to include some instructions for users to contact you to get help or a status update.